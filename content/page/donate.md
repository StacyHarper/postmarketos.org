title: "Donate to postmarketOS"
---
postmarketOS has been in development since 2016, and was published in 2017.
The blog posts often cover shiny new features, but that is just the tip of the
iceberg. A significant amount of work and time has to be spent on reviewing and
testing incoming patches, analyzing and fixing bugs in all kinds of code bases,
packaging new software releases, upstreaming patches, answering to issues,
making sure that the infrastructure works, writing blog posts. Donations allow
us to keep postmarketOS afloat in all aspects, and to bring it into a bright
future where *you* control *your phone* again and not the other way around.

Donations are accepted through the NLnet Foundation via
[bank transfer](#bank-transfer) and
[other mechanisms](https://nlnet.nl/donating/foundation-form.html) (as of
writing *BTC, BCH, NMC, PayPal* and *credit card*). Depending on your region,
they are [tax deductible](#tax-information).

Please confirm your donation by writing to
<b>donation@<span style="display:none">
</span>nlnet<span style="display:none">
</span>.nl</b>.
If you choose a different
method than bank transfer, then you must explicitly write that this donation
is intended for postmarketOS. You can add to the mail whether your donation is
anonymous (if not, please add
<b>donation@<span style="display:none">
</span>postmarketos<span style="display:none">
</span>.org</b>).

Processing the donations is currently not automated, so preferably this is used
for one larger amount rather than many small amounts.

Thank you for considering a donation!

### Bank transfer

<small>
**Reference:**<br>
Donation postmarketOS/TCC

**Name of account holder:**<br>
Stichting NLnet

**Account holder address:**<br>
Science Park 400<br>
1098 XH Amsterdam<br>
The Netherlands

**IBAN:**<br>
NL30 INGB 0007 2288 90

**BIC:**<br>
INGBNL2A

**Bank name:**<br>
ING BANK N.V.

**Bank address:**<br>
p/o box 1800<br>
1000 BV  Amsterdam<br>
The Netherlands
</small>

### Tax information
<small>
Stichting NLnet is an acknowledged public benefit organisation according to the
Netherlands tax authority (ANBI, Algemeen Nut Beogende Instelling). Your
donation may be tax deductible as a result. Please verify with your tax advisor
or local tax office. NLnet has EORI Number: NL-0079.89.398. Stichting NLnet is
registered at the Chamber of Commerce Amsterdam, number 41208365
</small>
