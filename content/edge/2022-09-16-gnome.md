title: "GNOME: gnome-shell gets uninstalled on upgrade"
date: 2022-09-16
---

For users of the regular GNOME on postmarketOS, there was a report that
gnome-shell gets uninstalled on upgrade.

Related issue: [pma#1701](https://gitlab.com/postmarketOS/pmaports/-/issues/1701)
