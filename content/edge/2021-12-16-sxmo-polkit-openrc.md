title: "Polkit fails to start with openrc in Sxmo installs"
date: 2021-12-16
---

Previous versions of the `sxmo-common` package (<1.6.0-7) erroneously added
polkit to the openrc default runlevel. This is not actually necessary since
polkit is started automatically by dbus later in `sxmo_xinit.sh` and
`sxmo_winit.sh`. As a result, openrc reports that the polkit service has failed
to start:

```
# cat var/log/messages  | grep polkit
2021-12-03 03:57:57 info polkitd[3443]: Started polkitd version 0.119
2021-12-03 03:57:58 warning supervise-daemon[3108]: /usr/lib/polkit-1/polkitd, pid 3443, exited with return code 0
2021-12-03 03:57:58 info supervise-daemon[3453]: Child command line: /usr/lib/polkit-1/polkitd --no-debug
...
2021-12-03 03:57:58 info polkitd[3471]: Started polkitd version 0.119
2021-12-03 03:57:58 warning supervise-daemon[3108]: /usr/lib/polkit-1/polkitd, pid 3471, exited with return code 0
2021-12-03 03:57:58 warning supervise-daemon[3108]: respawned "/usr/lib/polkit-1/polkitd" too many times, exiting
```

To resolve this, manually remove polkit from the default runlevel:

```
$ sudo rc-update del polkit
```

Later versions of the `sxmo-common` package do not add polkit to any openrc
runlevels.

### Also see:
- [pmaports#1325](https://gitlab.com/postmarketOS/pmaports/-/issues/1325)
