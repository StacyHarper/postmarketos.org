title: "v22.06 Release: The One Where We Started Using Release Titles"
title-short: "v22.06 Release"
date: 2022-06-12
preview: "2022-06/welcome-to-v22.06.jpg"
---

[![](/static/img/2022-06/welcome-to-v22.06.jpg){: class="wfull border" }](/static/img/2022-06/welcome-to-v22.06.jpg)

As we learned last winter, taking six weeks for following up on Alpine's
November release isn't ideal. While some free software hackers prefer to hack
on their favorite projects during the holidays, others take their well deserved
break far away from computers. We got it done regardless, and planned to cut
the release schedule in half to have it easier next time. The v22.06 release
right here was the trial run for the new schedule... and we made it. Here it
is, only three weeks after
[Alpine Linux 3.16](https://alpinelinux.org/posts/Alpine-3.16.0-released.html)
dropped and still [properly tested](https://gitlab.com/postmarketOS/pmaports/-/issues/1541):
the postmarketOS v22.06 release!

v22.06, like previous releases, is geared mainly towards Linux
enthusiasts; it may be a bit rough around the edges so expect some bugs. Help
identifying and resolving issues is always greatly appreciated.

## Supported devices

The amount of supported devices has been increased to 27 (from 25 since
[v21.12 SP4](/blog/2022/04/18/v21.12.4-release/)).

[#grid side#]

[![](/static/img/2022-06/shift6mq_thumb.jpg){: class="w300 border" }](/static/img/2022-06/shift6mq.jpg)
<br><span class="w300">
The SDM845 based SHIFT6mq sitting comfortably on a brick wall, running Phosh
and postmarketOS tweaks. Not in the picture: the super exciting
[background story](https://tuxphones.com/the-shift-we-need/)!
</span>

[![](/static/img/2022-06/pinetab_thumb.jpg){: class="w300 border" }](/static/img/2022-06/pinetab.jpg)
<br><span class="w300">
Martijn before fixing [pma#1558](https://gitlab.com/postmarketOS/pmaports/-/issues/1558): "main issue with pinetab audio is that I don't know where my pinetab is"
</span>

[#grid text#]

* ASUS MeMo Pad 7
* Arrow DragonBoard 410c
* BQ Aquaris X5
* Lenovo A6000
* Lenovo A6010
* Motorola Moto G4 Play
* Nokia N900 ([v21.12 SP1](/blog/2022/01/17/v21.12.1-release/))
* ODROID HC2
* OnePlus 6
* OnePlus 6T
* PINE64 PineBook Pro
* PINE64 PinePhone
* PINE64 PineTab
* PINE64 RockPro64
* Purism Librem 5
* SHIFT6mq <span class="new">new</span>
* Samsung Galaxy A3 (2015)
* Samsung Galaxy A5 (2015)
* Samsung Galaxy S III (GT-I9300 and SHW-M440S) <span class="new">new</span>
* Samsung Galaxy S4 Mini Value Edition
* Samsung Galaxy Tab 2 7.0" ([v21.12 SP4](/blog/2022/04/18/v21.12.4-release/))
* Samsung Galaxy Tab A 8.0
* Samsung Galaxy Tab A 9.7
* Wileyfox Swift
* Xiaomi Mi Note 2
* Xiaomi Pocophone F1
* Xiaomi Redmi 2

[#grid end#]

## Highlights

As with all releases, you won't find a description of each and every commit
that was done in postmarketOS edge and Alpine edge before the new release was
cut. That's what `git log` is for. But here are the highlights.

### Release Upgrades Now Possible

First of all, we are happy to announce that one can now upgrade from one
release to the next one. We made it work for Sxmo, Phosh and Plasma Mobile.
A script that
[runs every night](https://gitlab.com/postmarketOS/postmarketos-release-upgrade/-/pipelines)
ensures that this doesn't break for v21.12 -> v22.06 (and v22.06 -> edge to
prepare for the next release). While we don't officially support it, the script
even allows switching between all possible versions. Say from postmarketOS edge
to v22.06 if you are tired of living on the edge. Thanks to Alpine's package
manager apk it works fast and reliable. For now this is CLI based, in the
future it should be possible to hook this up to graphical apps like GNOME
Software and KDE Discover. More on upgrading
[in the wiki](https://wiki.postmarketos.org/wiki/Upgrade_to_a_newer_postmarketOS_release).

```
# apk add postmarketos-release-upgrade
# postmarketos-release-upgrade
>>> You are about to upgrade from v21.12 to v22.06
>>> 
>>> This will be done in the following steps:
>>> 1) upgrade packages of v21.12
>>> 2) dry run: upgrade packages to v22.06
>>> 3) upgrade packages to v22.06
>>> 4) prompt to reboot
>>> 
>>> A log and backup of your /etc/apk/repositories will be in:
>>> /var/lib/postmarketos-release-upgrade/2022-06-12-164229
>>> 
>>> It is strongly recommended to do this via SSH and in tmux/screen.
>>> More information: https://postmarketos.org/upgrade
>>> 
>>> This upgrade should work fine, but in the worst case your device
>>> may not boot anymore. Make backups of important data first!
>>> 
>>> Proceed with upgrade to v22.06? [y/N] 
```

[#grid side#]

[![](/static/img/2022-06/phosh_karlender.jpg){: class="w300 border" }](/static/img/2022-06/phosh_karlender.jpg)
<br><span class="w300">
Phosh 0.17.0 with the new postmarketOS welcome screen and karlender
</span>

[#grid text#]

### User Interfaces

* [Sxmo 1.9.0](https://lists.sr.ht/~mil/sxmo-announce/%3C3FV34P1BUD8B6.34I97X4SKLKHH%40yellow-orcess.my.domain%3E)
  supports device profiles, has improved bluetooth support and uses pipewire
  by default. The incall menu and audio management were improved, as well as
  superd being used to supervise user services.

* [Phosh 0.17.0](https://gitlab.gnome.org/World/Phosh/phosh/-/releases/v0.17.0)
  brings a slight style refresh and other minor improvements over the version
  0.15.0 we had in stable since
  [v21.12 SP2](/blog/2022/02/11/v21.12.2-release/). The
  [huge update](https://gitlab.gnome.org/World/Phosh/phosh/-/releases/v0.20.0_beta1)
  that is currently in the works will be backported via service pack once it's
  ready. GNOME is at 42, and a lot of GTK applications were ported to GTK4 and
  libadwaita. To fill the lack of a calendar application installed by default,
  we ship [karlender](https://gitlab.com/loers/karlender/) now.

* [Plasma Mobile Gear 22.04](https://plasma-mobile.org/2022/04/26/plasma-mobile-gear-22-04/)
  has a lot of great improvements as well, head over to their beautiful
  [new homepage](https://plasma-mobile.org/) to get an idea of what happened
  since Plasma Mobile Gear 21.12 which we shipped with postmarketOS v21.12.

[#grid end#]

### fwupd

PinePhone users who know what they are doing can now upgrade their modem to
[Biktor's alternative firmware](https://github.com/Biktorgj/pinephone_modem_sdk)
on v22.06. More [in the wiki](https://wiki.postmarketos.org/wiki/Fwupd).

### unudhcpd

When connecting your phone via USB to your computer, you are automatically
getting an IP from the phone's DHCP server and can then for example connect to
your phone via SSH. This is a feature since day one in postmarketOS, but now
the DHCP server has been replaced with a minimalistic one written specifically
for this use case, [unudhcpd](https://gitlab.com/postmarketOS/unudhcpd). It
responds to the DHCP request as fast and reliably as possible and doesn't get
confused if you plug the phone into two different PCs without reboot.

## In Other News

### Tweaks To Service Packs Approach

We reconsidered our approach to service packs. With v21.12 we made one service
pack each month, as planned in advance. Oftentimes we had pretty important
new features in there, like MMS in SP1 and support for the PPKB in SP2. But in
the last one, SP5 at some point we considered if we should even make the
service pack at all since there was not much to put in. But what do we do
instead, make a blog post that we don't make a service pack for the people who
expect one? And do we stop making service packs on schedule altogether?

So here is a little announcement: moving forward we will still make service
packs on schedule. But we may skip one if it is not worth the effort, then we
would just have a month without a service pack published and do something else
in that time (probably build something really cool in edge that we can't
backport yet). So don't panic if there might be a month where we skip
publishing a service pack.

### 5 Years Of postmarketOS
This concludes another release blog post. It's actually the fifth release, a
coincidence since now we make two per year, but the first stable release was
v20.05. And recently it has been the five years birthday of postmarketOS. Once
again we cannot stress enough how much of a team effort this has been. We thank
every single person who contributes to postmarketOS, to the upstream projects
like Alpine, the Linux kernel, Sxmo, Phosh, Plasma Mobile and so many more.
It's hard to keep track of the amazing things everybody is doing to make this
possible, but it's most definitively appreciated. Shout out to our friends from
other Linux Mobile distributions. Together we make the dream of being truly in
control of our smartphones a reality!
