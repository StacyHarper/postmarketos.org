# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
import os
import re

srcdir = os.path.normpath(os.path.realpath(__file__) + "/../..")

content_dir_blog = 'content/blog'
content_dir_edge = 'content/edge'
content_dir_page = 'content/page'

regex_split_frontmatter = re.compile(r'^---$', re.MULTILINE)
